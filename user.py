class User:

    #Constructs a new User instance
    def __init__(self, fName, lName, email, password):
        self.fName = fName
        self.lName = lName
        self.email = email
        self.password = password

    # Return a formatted string instance of a Fleet Manager Object
    def __str__(self):
        return self.fName + " " + self.lName + " " + self.email + " " + self.password

    # Returns the first name of a User instance
    def getFirstName(self):
        return self.fName

    # Returns the last name of a User instance
    def getLastName(self):
        return self.lName

    # Returns the email of a User instance
    def getEmail(self):
        return self.email

    # Returns the password of a User instance
    def getPassword(self):
        return self.password

    # Sets the first name of a User instance
    def setFirstName(self, fName):
        self.fName = fName

    # Sets the last name of a User instance
    def setLastName(self, lName):
        self.lName = lName

    # Sets the email of a User instance
    def setEmail(self, email):
        self.email = email

    # Sets the password of a User instance
    def setPassword(self, password):
        self.password = password