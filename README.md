# README

### Summary

* Back end files for the supply-side server (demand.team24.softwareengineeringii.com).

### Navigation

* `HTTPrequest_supply.py`
    * Receives web service requests and performs tasks accordingly (sending data to database, logging in user, etc.).
* `routesteps.py`
    * Mapping file.
* `user.py`
    * User object.
* `vehicle.py`
    * Vehicle object.

### Team members

* TM1 - Sydney Mack
* TM2 - Mariah Sager
* TM3 - Keldon Boswell
* TM4 - Colby Hayes
* TM5 - Karla Salto
