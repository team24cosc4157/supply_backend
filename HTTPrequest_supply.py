# Python 3

# NOTE: I (Mariah) retyped this manually from Amanda's documentation. I'll check for typos.

import http.server
from http.server import BaseHTTPRequestHandler
import json
import urllib.parse
# import user.py
import pymysql.cursors
import ctypes  # An included library with Python install.


def connection():
    # Connect to the database
    connection = pymysql.connect(host='127.0.0.1',
                                 port=6024,
                                 user='root',
                                 password='password',
                                 db='test',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    # The name of these methods cannot change, they must be 'do_(REQUEST TYPE HERE)' e.g. do_OPTIONS or do_DELETE.
    # I've shown the methods for POST and GET, but for a more comprehensive list of the request types, see -> https://www.restapitutorial.com/lessons/httpmethods.html
    # a method to handle all POST requests (You can check the path of the request to differentiate between requests)
    def do_POST(self):
        # get the path of the request
        path = self.path

        # check the path of the request
        if path == "//login":

            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '"')
            print('Content-Type:', self.headers['content-type'])

            # This is how you read the body of the request:

            # First grab the length of the body and read
            # that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)

            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print('Body:', body)

            # convert bytes to string using .decode()
            print("Body (String): ", body.decode())
            conn = connection()
            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            user_dictionary = json.loads(body)
            # Grabbing the users email
            email = user_dictionary['email']
            # Grabbing the users password
            password = user_dictionary['Password']
            # userObject = user(first_name, last_name, email, password)
            with conn.cursor() as cursor:
                # SQL query to grab the instances where the email and password are the same as the user enterered-> Meaning it is a registered user 
                grab_login = "select email, password from fleetManager where email =%s and password =%s"
                login_val = (email, password)
                check_login = cursor.execute(grab_login, login_val)
                print(check_login)
                if check_login == 1:
                    print("Sending a 200 to the frontend!")
                    response_code_login = 200
                    # must commit changes
                    conn.commit()
                    self.send_response(response_code_login)
                # email is not registered so send a 300 to the JS letting them know to tell the user to register
                else:
                    print()
                    response_code_login = 300
                    self.send_response(response_code_login)
                    conn.close()
            # To access a specific key from the dictionary:
            # print(user)

            # Now you can get to the important stuff (DB, API calls, etc.)

            # If you need to make API calls use the python 'requests' library -> https://requests.readthedocs.io/en/master/

            # For connecting to a MySQL DB, use 'pymysql' library -> https://pymysql.readthedocs.io/en/latest/

            # For connecting to MongoDB, use 'pymongo' library -> https://api.mongodb.com/python/current/

            # Now send a response code!
            # For a comprehensive list of response codes and what they mean, see -> https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
            # Very important, or your response will be slow!
            # Signify that you are done sending headers
            self.end_headers()

            # There are a few ways to send a response
            # Or you could format your json in a string like:
            res = '{"success": true, "otherParams": "here"}'
            # Use single quotes because JSON is only valid if it has double quotes for keys and strings!

            # If you don't want to format your JSON, make a dictionary, and
            # convert it to a string with 'json.dumps'
            responseDict = {}
            responseDict['success'] = True
            res = json.dumps(responseDict)

            # Whichever way you choose, you need to have a string that you can convert to bytes because the body only receives bytes.

            bytes_str = res.encode('utf-8')
            self.wfile.write(bytes_str)
            # Check as many paths as you have POST requests
        elif path == "//register":
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '"')
            print('Content-Type:', self.headers['content-type'])
            # This is how you read the body of the request:
            # First grab the length of the body and read
            # that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)
            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print('Body:', body)
            # convert bytes to string using .decode()
            print("Body (String): ", body.decode())
            conn = connection()
            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            user_dictionary = json.loads(body)
            # Grabbing the users first name
            first_name = user_dictionary['First Name']
            # Grabbing the users last name
            last_name = user_dictionary['Last Name']
            # Grabbing the users email
            email = user_dictionary['Email']
            # Grabbing email
            password = user_dictionary['Password']
            # userObject = user(first_name, last_name, email, password)
            # If the email is not  duplicate then we want to go ahead and insert it into the DB
            with conn.cursor() as cursor:
                duplicate_email = "select email from fleetManager where email =%s"
                number_of_rows_affected = cursor.execute(duplicate_email, (email))
                response_code = 200
                if (number_of_rows_affected == 0):
                    print("Email is not a duplicate, can proceed")
                    sql = "INSERT INTO fleetManager (fName, lName, email, password ) VALUES (%s, %s, %s, %s)"
                    val = (first_name, last_name, email, password)
                    # create a new record by submitting the users info
                    cursor.execute(sql, val)
                    # must commit changes
                    print("Executing query")
                    conn.commit()
                    self.send_response(response_code)
                else:
                    response_code = 300
                    self.send_response(response_code)
                    print("Email is a duplicate!")
                    conn.close()

            # To access a specific key from the dictionary:
            # print(user)
            # Now you can get to the important stuff (DB, API calls, etc.
            # If you need to make API calls use the python 'requests' library -> https://requests.readthedocs.io/en/master/
            # For connecting to a MySQL DB, use 'pymysql' library -> https://pymysql.readthedocs.io/en/latest/
            # For connecting to MongoDB, use 'pymongo' library -> https://api.mongodb.com/python/current/
            # Now send a response code!
            # For a comprehensive list of response codes and what they mean, see -> https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
            # If everything went well, it will send a 200, otherwise a 300 -> and if it doesn't connect to any endpoint will send a 404
            self.send_response(response_code)
            # Very important, or your response will be slow!
            # Signify that you are done sending headers
            self.end_headers()
            # There are a few ways to send a response
            # Or you could format your json in a string like:
            res = '{"success": true, "otherParams": "here"}'
            # Use single quotes because JSON is only valid if it has double quotes for keys and strings!
            # If you don't want to format your JSON, make a dictionary, and
            # convert it to a string with 'json.dumps''
            responseDict = {}
            responseDict['success'] = True
            res = json.dumps(responseDict)
            # Whichever way you choose, you need to have a string that you can convert to bytes because the body only receives bytes.
            bytes_str = res.encode('utf-8')
            self.wfile.write(bytes_str)
            # Check as many paths as you have POST requests
        elif path == "//vehicleRequest":
            # You can get all headers via self.headers
            # or identify which header you want to get
            print('Headers:"', self.headers, '"')
            print('Content-Type:', self.headers['content-type'])
            # This is how you read the body of the request:
            # First grab the length of the body and read
            # that many characters
            length = int(self.headers['content-length'])
            body = self.rfile.read(length)
            # at this point you can print the whole body
            # if you want (good for debugging)
            # at this point body is in bytes
            print('Body:', body)
            # convert bytes to string using .decode()
            print("Body (String): ", body.decode())
            conn = connection()
            # How to convert the body from a string to a dictionary
            # use 'loads' to convert from byte/string to a dictionary!
            user_dictionary = json.loads(body)
            orderNum = user_dictionary['OrderID']
            streetName = user_dictionary['Street Name']
            cityName = user_dictionary['City Name']
            state = user_dictionary['State']
            zip_code = user_dictionary['Zip Code']
            service_type = user_dictionary['Service Type']
            print(user_dictionary)
            # Grabbing the users email
            self.send_response(200, "Hooray! You have reached the Vehicle request")
            self.end_headers()
        else:
            self.send_response(404)
            self.end_headers()


# else:
# if the path did not match any of
# your known requests return a 404 Not Found
# self.send_response(404)
# self.end_headers()

def do_GET(self):
    # similar logic here, but for GET requests

    # only difference may be that you may have query parameters
    # i.e. /route?param=value
    # To get these params, use urllib.parse (Python 3.x~)

    # if your query is something like '/?param=value&otherParam=value', use
    # something like this to parse everything but the first two characters:
    params = urllib.parse.parse_qs(self.path[2:])  # returns {'param': ['value'], 'otherParam': ['value']}

    # However, if you are looking for a specific path with params like '/route?param=value'
    # You will need to check the first characters (before the '?') so with '/route' (6 chars including the '/') do:
    if self.path[:6] == '/route':

        # and then instead of [2:] when substringing self.path, substring
        # it from the 7th char onwards since the '?' is the seventh character!
        params = urllib.parse.parse_qs(self.path[7:])

        print(params)
        # params will hold a variable with the contents: {'param': ['value']}
        # unfortunately, this places the values in an array, so access using:
        param = params['param'][0]
        # will return 'myUsername'

        print(param)

        # You always need to send a response code
        # and signify that you are done sending headers
        self.send_response(200)
        self.end_headers()
        # but you don't necessarily need to send a response body
    else:
        self.send_response(404)
        self.end_headers()


def main():
    # Define the port your server will run on:
    # Using 4001 as an example! Yours may run on another port!
    # Consult with Devops Coordinator to find out which port
    # your server should be running on!
    port = 4024
    # Create an http server using the class and port you defined
    httpServer = http.server.HTTPServer(('', port), SimpleHTTPRequestHandler)
    print("Running on port", port)

    # this next call is blocking! So consult with Devops Coordinator for
    # instructions on how to run without blocking other commands from being
    # executed in your terminal!
    httpServer.serve_forever()


if __name__ == "__main__":
    main()
