class Vehicle(object):
    make = ""
    model = ""
    year = ""
    vin = ""
    license_plate = ""
    long_lat = []
    battery_level = float
    mileage = int
    last_date_brake_fluid = ""
    last_date_windshield_fluid = ""

    #Creating a Vehicle Object
    def __init__(self, make, model, year, vin, license_plate, long_lat, battery_level, mileage, last_date_brake_fluid, last_date_windshield_fluid):
        self.make = make
        self.model = model
        self.year = year
        self.vin = vin
        self.licensePlate = license_plate
        self.long_lat = long_lat
        self.battery_level = battery_level
        self.mileage = mileage
        self.last_date_brake_fluid = last_date_brake_fluid
        self.last_date_windshield_fluid = last_date_windshield_fluid

    

    def get_battery_level(self):
        return self.battery_level

    def get_mileage(self):
        return self.mileage

    def get_last_date_brake_fluid(self):
        return self.last_date_brake_fluid

    def get_last_date_windshield_fluid(self):
        return self.last_date_windshield_fluid

    #string representation of the object
    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)


car1 = Vehicle('Chevy', 'Cruze', '2014', '123456', 'ABC123', [-97.7431, 30.2672], 1.00, 654132, '1/1/2020', '1/1/2020')
print(car1)
