import requests
import time


def main():
    my_route = get_route()
    print("Route Received")
    print(my_route)
    print(get_eta())
    print_route(my_route)


#Getting a route from an http get request and converting it to an array of longlat arrays
def get_route():
    r = requests.get('https://api.mapbox.com/directions/v5/mapbox/driving/-84.518641,39.134270;-84.512023,39.102779?&steps=true&geometries=geojson&access_token=pk.eyJ1Ijoic3lkbmV5ZWxpc2VlIiwiYSI6ImNrNjc5ZnhjdDBwOTAza25zcHlzNmU4ajQifQ.gJyoVDR-ZAxrOqpzomcpsQ')
    route = r.json()

    coordinates = []
    for step in route['routes'][0]['legs'][0]['steps']:
        for coord in step['geometry']['coordinates']:
            coordinates.append(coord)
    return coordinates


#Getting the estimated arrival time from the route and converting it to hours and minutes
def get_eta():
    r = requests.get('https://api.mapbox.com/directions/v5/mapbox/driving/-84.518641,39.134270;-84.512023,39.102779?&steps=true&geometries=geojson&access_token=pk.eyJ1Ijoic3lkbmV5ZWxpc2VlIiwiYSI6ImNrNjc5ZnhjdDBwOTAza25zcHlzNmU4ajQifQ.gJyoVDR-ZAxrOqpzomcpsQ')
    distance = r.json()
    total_eta = distance["routes"][0]["duration"]
    eta_hours = int(total_eta // 60)
    eta_minutes = int(((total_eta / 60) - eta_hours) * 60)
    converted_eta = str(eta_hours) + " hours and " + str(eta_minutes) + " minutes"
    return "Estimated time of arrival: " + converted_eta


#Prints an updated location for the vehicle every 10 seconds
def print_route(coordinates):
    start_time = time.time()
    for step in coordinates:
        print('Vehicle is at: ' + str(step))
        time.sleep(10.0 - ((time.time() - start_time) % 10.0))
    print("Route Completed")


main()
